const ARXIVER_API_URL = "http://localhost:48080/";

var Library = React.createClass({
  getInitialState: function() {
    return {
      queue: [],
      read: [],
    };
  },
  refresh: function() {
    this.componentDidMount();
  },
  componentDidMount: function() {
    const url = ARXIVER_API_URL + "getlibrary";
    $.ajax({
      url: url,
      dataType: "json",
    }).done(function(data) {
      console.log("Fetched library")
      var queue = data["queue"].map((cur) =>
        <li>
          <ArticleCard metadata={cur["metadata"]}
                       buttons={["view", "remove", "read"]}
                       onRemove={() => this.refresh()}
                       onRead={() => this.refresh()} />
        </li>
      );
      var read = data["read"].map((cur) =>
        <li>
          <ArticleCard metadata={cur["metadata"]}
                       buttons={["view", "remove", "unread"]}
                       onRemove={() => this.refresh()}
                       onUnread={() => this.refresh()} />
        </li>
      );
      this.setState({
        queue: queue,
        read: read,
      });
    }.bind(this));
  },
  render: function() {
    return (
      <div>
        <h1>Library Page</h1>
        <div className="lib-shelves">
          <LibList name="Queue" list={this.state.queue} />
          <LibList name="Already Read" list={this.state.read} />
        </div>
      </div>
    );
  },
});
