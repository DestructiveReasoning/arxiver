var SearchBar = React.createClass({
  render: function() {
    return (
      <div className="search">
        <form className="search-enter" onSubmit={this.props.onSearch}>
          <input type="text" className="search-text" id={this.props.id}/>
          <button type="submit"><i className="material-icons search-icon">search</i></button>
        </form>
      </div>
    );
  },
});
