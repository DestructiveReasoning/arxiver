var LibList = React.createClass({
  render: function() {
    return (
      <div className="lib-list">
        <span className="lib-list-label">
          <b>{this.props.name}</b> <span className="lib-list-length">({this.props.list.length} articles)</span>
        </span>
        <ul className="article-list">
          {this.props.list}
        </ul>
      </div>
    );
  },
});
