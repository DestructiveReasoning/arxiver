const ARXIVER_API_URL = "http://localhost:48080/";

var ArticleCard = React.createClass({
  getInitialState: function() {
    return {
      expanded: false,
    };
  },
  buttonTypes: {
    view: {
      name: "View",
      action: function() {
        window.open(this.props.metadata["link"], "_blank");
      },
    },
    addToQueue: {
      name: "Add to Queue",
      action: function() {
        $.ajax({
          method: "POST",
          url: ARXIVER_API_URL + "addtoqueue",
          data: JSON.stringify(this.props.metadata),
          contentType: "text/plain",
          dataType: "json",
          crossDomain: true,
        }).done(function(data) {
          var thisNode = ReactDOM.findDOMNode(this);
          $(thisNode).addClass("article-card-animate");
          this.props.onAdd();
        }.bind(this));
      },
    },
    remove: {
      name: "Remove",
      action: function() {
        $.ajax({
          method: "POST",
          url: ARXIVER_API_URL + "removefromlib",
          data: JSON.stringify(this.props.metadata),
          contentType: "text/plain",
          dataType: "json",
          crossDomain: true,
        }).done(function(data) {
          this.props.onRemove();
        }.bind(this));
      },
    },
    read: {
      name: "Mark Read",
      action: function() {
        $.ajax({
          method: "POST",
          url: ARXIVER_API_URL + "markread",
          data: JSON.stringify(this.props.metadata),
          contentType: "text/plain",
          dataType: "json",
          crossDomain: true,
        }).done(function(data) {
          this.props.onRead();
        }.bind(this));
      },
    },
    unread: {
      name: "Mark Unread",
      action: function() {
        $.ajax({
          method: "POST",
          url: ARXIVER_API_URL + "markunread",
          data: JSON.stringify(this.props.metadata),
          contentType: "text/plain",
          dataType: "json",
          crossDomain: true,
        }).done(function(data) {
          this.props.onUnread();
        }.bind(this));
      },
    },
  },
  onClick: function() {
    this.setState({
      expanded: !this.state.expanded,
    });
  },
  render: function() {
    var buts = this.props.buttons.map((cur) =>
      <a className="article-card-button" onClick={this.buttonTypes[cur].action.bind(this)}>{this.buttonTypes[cur].name}</a>
    );
    return (
      <div>
        <div className="article-card">
          <a className="article-card-expand" onClick={this.onClick}><b>{this.props.metadata["title"]}</b><br />
          {this.props.metadata["authors"].join(", ")}<br />
          <span className="arxiv-date-label">Last updated: {this.props.metadata["date"]}</span><br />
          <span className="arxiv-category-label">{this.props.metadata["category"]}</span></a>
          {this.state.expanded != 0 &&
            <div className="article-card-abstract">
              <p>{this.props.metadata["abstract"]}</p>
              <div className="article-card-buttons">
                {buts}
              </div>
            </div>
          }
        </div>
      </div>
    );
  }
});
