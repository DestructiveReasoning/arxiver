from bson.json_util import dumps as bson_dumps
from flask import Flask
from flask import request
from flask_cors import CORS, cross_origin
import json

import arXiv
from mongodbexecutor import MongoDBExecutor

application = Flask(__name__)
cors = CORS(application)
application.config['CORS_HEADERS'] = 'Content-Type'

dbExecutor = None

@application.route('/all', methods=['GET'])
@cross_origin()
def search_general():
    start = request.args.get("start", default=0, type=int)
    amount = request.args.get("amount", default=10, type=int)
    dict_results = arXiv.get_all_articles(start, amount)
    return json.dumps(dict_results, ensure_ascii=False)

@application.route('/search', methods=['GET'])
@cross_origin()
def search_query():
    start = request.args.get("start", default=0, type=int)
    amount = request.args.get("amount", default=10, type=int)
    query = request.args.get("search_query", default="all")
    dict_results = arXiv.get_query_articles(query, start=start, amount=amount)
    return json.dumps(dict_results, ensure_ascii=False)

@application.route('/addtoqueue', methods=['POST'])
@cross_origin()
def add_to_queue():
    json_received = request.data.decode('utf-8')
    article = json.loads(json_received)
    dbExecutor.lib_add(article)
    return json.dumps({})

@application.route('/removefromlib', methods=['POST'])
@cross_origin()
def remove_from_lib():
    json_received = request.data.decode('utf-8')
    article = json.loads(json_received)
    dbExecutor.lib_remove(article)
    return json.dumps({})

@application.route('/markread', methods=['POST'])
@cross_origin()
def mark_read():
    json_received = request.data.decode('utf-8')
    article = json.loads(json_received)
    dbExecutor.lib_mark_read(article)
    return json.dumps({})

@application.route('/markunread', methods=['POST'])
@cross_origin()
def mark_unread():
    json_received = request.data.decode('utf-8')
    article = json.loads(json_received)
    dbExecutor.lib_mark_unread(article)
    return json.dumps({})

@application.route('/getlibrary', methods=['GET'])
@cross_origin()
def retrieve_library():
    lib = dbExecutor.lib_retrieve_all()
    return bson_dumps(lib)

def setup():
    global dbExecutor
    dbExecutor = MongoDBExecutor()

if __name__ == "__main__":
    setup()
    application.run(port=48080)
