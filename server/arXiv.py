import urllib
import urllib.request as request
from bs4 import BeautifulSoup

ARXIV_API = "http://export.arxiv.org/api/query"

def get_all_articles(start, amount):
    return get_query_articles("all", start=start, amount=amount)

def get_query_articles(query, start=0, amount=10):
    query = get_arxiv_query(query, start=start, amount=10)
    print("Executing GET " + query)
    data = request.urlopen(query).read()
    xml = BeautifulSoup(data, "lxml")
    entries = xml.find_all("entry")
    return [parse_arxiv_entry(entry) for entry in entries]

def parse_arxiv_entry(entry):
    res = {}
    authors = entry.find_all("author")
    authors = [author.text.strip() for author in authors]
    res["authors"] = authors
    res["title"] = entry.find("title").text.strip()
    res["abstract"] = entry.find("summary").text.strip()
    res["date"] = entry.find("updated").text.strip().split("T")[0]
    res["link"] = entry.find("link", {"rel": "alternate"}).get("href")
    res["category"] = entry.find("arxiv:primary_category").get("term")
    return res

def get_arxiv_query(query, start=0, amount=10):
    query = '"{}"'.format(query.replace('-', ' '))
    formatted_query = urllib.parse.quote(query)
    return "{}?search_query={}&start={}&max_results={}&sortBy=lastUpdatedDate&sortOrder=descending".format(ARXIV_API, formatted_query, start, amount)
