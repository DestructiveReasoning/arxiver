# arXiver

arXiver is a tool for organizing, exploring, and managing arXiv articles. It was
partially inspired by the [arxiv sanity
preserver](http://www.arxiv-sanity.com), however it has more of a focus on
managing a personal collection of arXiv papers of interest.
