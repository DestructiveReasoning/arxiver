const ARXIVER_API_URL = "http://localhost:48080/";
const ARXIVER_ALL_API_ENDPOINT = ARXIVER_API_URL + "all?";
const ARXIVER_SEARCH_API_ENDPOINT = ARXIVER_API_URL + "search?";

function resetPage() {
  window.scrollTo(0, 0);
}

var BrowsePage = React.createClass({
  getInitialState: function() {
    return {
      page: 0,
      amount: 10,
      articles: [],
      hasNext: false,
      hasPrev: false,
      loading: false,
    };
  },
  createAPIQuery: function() {
    const suffix = "start=" + (this.state.page * this.state.amount) + "&amount=" + this.state.amount;
    if(this.props.query.length == 0) {
      return ARXIVER_ALL_API_ENDPOINT + suffix;
    }
    console.log("Searching arxiv...");
    return ARXIVER_SEARCH_API_ENDPOINT + "search_query=" + this.props.query + "&" + suffix;
  },
  onClick: function(inc) {
    this.setState({
      page: this.state.page + inc,
    });
  },
  onSubmit: function() {
    var page = parseInt(document.getElementById(this.props.query + "-page-enter").value);
    console.log("Jumping to page: " + page);
    document.getElementById(this.props.query + "-page-enter").value = "";
    this.setState({
      page: page - 1,
    });
  },
  getResults: function() {
    var url = this.createAPIQuery();
    this.setState({loading: true});
    $.ajax({
      url: url,
      dataType: "json",
    }).done(function(data) {
      console.log("Got response:");
      if(data.length > 0) {
        console.log(data[0]["title"]);
        console.log(data[0]["authors"]);
      } else {
        console.log("<no articles>");
      }
      this.setState({
        articles: data,
        hasNext: (data.length == this.state.amount),
        hasPrev: (this.state.page > 0),
        loading: false,
      }, resetPage);
    }.bind(this));
    return;
  },
  componentDidMount: function() {
    this.getResults();
  },
  componentDidUpdate: function(prevProps, prevState) {
    if(this.props.query != prevProps.query || this.state.page != prevState.page){
      this.setState({
        page: (this.props.query == prevProps.query) ? this.state.page : 0,
        articles: [],
        hasNext: false,
        hasPrev: false,
      }, this.getResults);
    }
  },
  render: function() {
    if(this.state.loading) {
      return <LoadingCircle />;
    }
    if(this.state.articles.length == 0) {
      return <div>No articles found.</div>;
    }
    console.log("Current query: " + this.props.query);
    const articles = this.state.articles.map((cur, index) => {
      return <li><ArticleCard metadata={cur} buttons={["view", "addToQueue"]} onAdd={() => 0} onRemove={() => 0} onRead={() => 0}/></li>;
    });
    return (
      <div>
        <h1>{this.props.title}</h1>
        <ul className="article-list">
          {articles}
        </ul>
        <div className="page-bar">
          <form className="page-bar-form" onSubmit={this.onSubmit}>
            {this.state.hasPrev &&
              <a className="page-bar-button" onClick={() => this.onClick(-1)}><i className="fa fa-chevron-left"></i></a>
            }
            <input type="text" placeholder={this.state.page + 1} onsubmit={() => this.onSubmit()} id={this.props.query + "-page-enter"}></input>
            <input type="submit" className="page-bar-submit" id={this.props.query + "-page-bar-submit"} value="Go"></input>
            {this.state.hasNext &&
              <a className="page-bar-button" onClick={() => this.onClick(1)}><i className="fa fa-chevron-right"></i></a>
            }
          </form>
        </div>
      </div>
    );
  },
});
