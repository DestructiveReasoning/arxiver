const LINK = 0;
const SEARCH = 1;

var App = React.createClass({
  getInitialState: function() {
    return {
      selected: 0,
    };
  },
  onClick: function(i) {
    if (i == this.state.selected) {
      return;
    }
    this.setState({
      selected: i,
    });
  },
  onSearch: function() {
    this.setState({
      selected: 1,
    }, this.render);
  },
  render: function() {
    const navs = this.props.elems.map((cur, index) => {
        if(cur["type"] == LINK) {
          return (
            <li className="nav-link" id={cur["id"]}><a href="#" onClick={() => this.onClick(index)}>{cur["name"]}</a></li>
          )
        } else if(cur["type"] == SEARCH) {
          return (
            <li className="nav-link" id={cur["id"]}><SearchBar onSearch={this.onSearch} id="main-search-id"/></li>
          )
        }
      }
    );
    var page = this.props.elems[this.state.selected].render();
    if(this.state.selected == 1) {
      page = <BrowsePage title="Search Results" query={document.getElementById("main-search-id").value} />;
    }
    return (
      <div>
        <div className="nav-bar">
          <ul className="nav-links">
            {navs}
          </ul>
        </div>
        <div id="app">
          {page}
        </div>
      </div>
    );
  }
});

var navElements = [
  {
    name: "arXiver",
    id: "nav-link-home",
    type: LINK,
    render: function() {
      return (
        <BrowsePage title="Front Page" query=""/>
      );
    },
  },
  {
    name: "Search",
    id: "search-form",
    type: SEARCH,
    render: function() {
      <BrowsePage title="Search Results" query={document.getElementById("main-search-id").value} />
    },
  },
  {
    name: "Library",
    id: "nav-link-library",
    type: LINK,
    render: function() {
      return (
        <Library />
      );
    }
  },
  {
    name: "Front Page",
    id: "nav-link-front-page",
    type: LINK,
    render: function() {
      return (
        <BrowsePage title="Front Page" query=""/>
      );
    },
  },
];

ReactDOM.render(<App elems={navElements} />, document.getElementById("root"));
