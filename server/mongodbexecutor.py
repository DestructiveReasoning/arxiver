import pymongo
import time

class MongoDBExecutor(object):
    def __init__(self):
        self.client = pymongo.MongoClient("mongodb://localhost:27017/")
        self.db = self.client["arxiver"]
        self.lib = self.db["library"]
        self.return_filter = {"state": 1, "metadata": 1, "_id": 0, "date-added-to-lib": 1, "arxiv-id": 1}

    def lib_add(self, article):
        doc = create_article_document(article)
        if self.lib.find_one({"arxiv-id": doc["arxiv-id"]}) is None:
            self.lib.insert_one(doc)

    def lib_remove(self, article):
        doc = create_article_document(article)
        if self.lib.find_one({"arxiv-id": doc["arxiv-id"]}) is not None:
            self.lib.delete_one({"arxiv-id": doc["arxiv-id"]})

    def lib_mark_read(self, article):
        doc = create_article_document(article)
        if self.lib.find_one({"arxiv-id": doc["arxiv-id"], "state": "queue"}) is not None:
            date = time.time()
            modifications = {"state": "read", "date-added-to-lib": date}
            self.lib.update_one({"arxiv-id": doc["arxiv-id"], "state": "queue"}, {"$set": modifications})

    def lib_mark_unread(self, article):
        doc = create_article_document(article)
        if self.lib.find_one({"arxiv-id": doc["arxiv-id"], "state": "read"}) is not None:
            date = time.time()
            modifications = {"state": "queue", "date-added-to-lib": date}
            self.lib.update_one({"arxiv-id": doc["arxiv-id"], "state": "read"}, {"$set": modifications})

    def lib_retrieve_all(self):
#        if "library" not in self.db.list_collection_names():
#            return {}
        collection = {}
        collection["queue"] = self.lib.find({"state": "queue"}, self.return_filter).sort("date-added-to-lib", -1)
        collection["read"] = self.lib.find({"state": "read"}, self.return_filter).sort("date-added-to-lib", -1)
        if collection["queue"] is None:
            collection["queue"] = {}
        if collection["read"] is None:
            collection["read"] = {}
        return collection

def create_article_document(article, state="queue"):
    arxiv_id = get_arxiv_id(article)
    date = int(time.time())
    return {"state": state, "metadata": article, "date-added-to-lib": date, "arxiv-id": arxiv_id}

def get_arxiv_id(article):
    link = article["link"]
    return link.split("http://arxiv.org/abs/")[-1]
